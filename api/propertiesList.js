import request from '@/api/request.js'

export default () => {
  return request({
    data: {
      query: `
      {
        Properties{
          select {
            id(orderBy: DESC)
            price
            info
            propertyFeature {
              bedRoom
              park
              storey
              bathRoom
              area
              kitchen
            }
            agent {
              id
              email
              name
              tel
              position
            }
            location{
              longitude
              latitude
            }
            bgImage
            images
            description
            address
            name
            currency
            type{
              id
              name
            }
            commercialType
            createdAt
            priority
            brief
            language
          }
        }
      }
      `
    }
  })
}
