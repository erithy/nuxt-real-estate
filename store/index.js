export const state = () => ({
  user: {
    locale: 'en',
    logo: {}
  },
  header: [],
  homeNews: [],
  home: { firstList: [], secondList: [] },
  homeSlide: {},
  contact: {}
})

export const mutations = {
  SET_LANG (state, locale) {
    if (state.locales.includes(locale)) {
      state.locale = locale
    }
  },
  initailHomeData (state, payload) {
    state.homeNews = payload.NewsAndEvents.select
    state.home.firstList = payload.firstList.select
    state.home.secondList = payload.secondList.select
    state.homeSlide = payload.PageConfigurations.select[0]
    state.user.logo = payload.logo.jsonValue
    if (payload.contact.select.length) { state.contact = payload.contact.select[0].jsonValue }
  },
  setEssentail (state, payload) {
    state.user.logo = payload.logo.jsonValue
    if (payload.contact.select.length) { state.contact = payload.contact.select[0].jsonValue }
  }
}
