
import request from '@/api/request.js'

export default (keywords) => {
  const lang = (keywords.lang === 'en') ? 'ENGLISH' : (keywords.lang === 'kh') ? 'KHMER' : 'CHINESE'
  return request({
    data: {
      query: `
        {
          ProvinceDistricts(where: {language: {EQ: ${lang}}}) {
            select {
              id
              description
              language
              name
            }
          }
          PageConfigurations(where: {language: {EQ: ${lang}}}) {
            select {
              shortDescription
              name
              language
              slideImage
              info
              id
            }
          }
          aboutVision:KeyValues(where: {key: {EQ: "aboutVision"} language: {EQ: ${lang}} }) {
            select {
              id
              key
              value
              jsonValue
              language
            }
          }
          contact:KeyValues(where: {key: {EQ: "contact"} language: {EQ: ${lang}} }) {
            select {
              id
              key
              value
              jsonValue
              language
            }
          }
          logo:KeyValue(id:215) {
            id
            key
            value
            jsonValue
            language
          }
          NewsAndEvents(where: {displayOnHome: {EQ: true} language: {EQ: ${lang}} } page: {
              limit: 5
              start: 1
            }) {
              select {
                id
                title
                content
                createdAt
                images
                bgImage
                language
                brief
                info
                language
              }
            }
              firstList:Properties(where: { language: {EQ: ${lang}} } page: {
                limit: 6
                start: 1
              }){
                total
                pages
                select {
                  id
                  price
                  info
                  description
                  propertyFeature {
                    bedRoom
                    park
                    storey
                    bathRoom
                    area
                    kitchen
                  }
                  agent {
                    id
                    email
                    name
                    tel
                    position
                  }
                  bgImage
                  images
                  address
                  name
                  currency
                  type{
                    id
                    name
                  }
                  location{
                    longitude
                    latitude
                  }
                  commercialType
                  createdAt(orderBy: DESC)
                  priority
                  brief
                  language
                }
              }
    secondList: Properties(where: {displayOnHome: { EQ: true } language: {EQ: ${lang}} } page: {
                limit: 6
                start: 1
              }){
                total
                pages
                select {
                  id(orderBy: DESC)
                  price
                  displayOnHome
                  info
                  description
                  propertyFeature {
                    bedRoom
                    park
                    storey
                    bathRoom
                    area
                    kitchen
                  }
                  agent {
                    id
                    email
                    name
                    tel
                    position
                  }
                  bgImage
                  images
                  address
                  name
                  currency
                  type{
                    id
                    name
                  }
                  location{
                    longitude
                    latitude
                  }
                  commercialType
                  createdAt
                  priority
                  brief
                  language
                }
              }
        }
        `
    }
  })
    .then(response => response.data)
}
