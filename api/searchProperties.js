import request from '@/api/request.js'

export default (payload) => {
  const commercialType = () => {
    if (!payload.commercialType) { return '' }
    return `commercialType: {EQ: ${payload.commercialType}}`
  }
  const propretyTypes = () => {
    if (!payload.propertyType) { return '' }
    return `type: {name: {EQ: "${payload.propertyType}"}}`
  }
  const locations = () => {
    if (!payload.location) { return '' }
    return `address: {LIKE: "${payload.location}"}`
  }
  const price = () => {
    if (!payload.price) { return '' }
    if (!payload.price[1] && !payload.price[0]) { return '' }
    return `price:{
      LT:${payload.price[1]}
    }
    AND:{
      price:{
        GE:${payload.price[0]}
      }
    }`
  }
  const lang = (payload.lang === 'en') ? 'ENGLISH' : (payload.lang === 'kh') ? 'KHMER' : 'CHINESE'

  return request({
    data: {
      query: `
      {
        contact:KeyValues(where: {key: {EQ: "contact"} language: {EQ: ${lang}} }) {
          select {
            id
            key
            value
            jsonValue
            language
          }
        }
        logo:KeyValue(id:215) {
          id
          key
          value
          jsonValue
          language
        }
        header:KeyValues(where: {key: {EQ: "PropertiesTitle"} language: {EQ: ${lang}} }) {
          select {
            id
            key
            value
            jsonValue
            language
          }
        }
        ProvinceDistricts(where: {language: {EQ: ${lang}} }) {
          select {
            id
            name
            description
            language
          }
        }
        PropertyTypes(where: {language: {EQ: ${lang}} }) {
          select {
            name
            id
            language
            image
            description
          }
        }
        Properties(
            where: {language: {EQ: ${lang}} ${commercialType()} ${propretyTypes()} ${locations()} ${price()}}
          ){
          select {
            id(orderBy: DESC)
            price
            info
            propertyFeature {
              bedRoom
              park
              storey
              bathRoom
              area
              kitchen
            }
            agent {
              id
              email
              name
              tel
              position
            }
            location{
              longitude
              latitude
            }
            bgImage
            images
            description
            address
            name
            currency
            type{
              id
              name
            }
            commercialType
            createdAt
            priority
            brief
            language
          }
        }
      }
      `
    }
  }).then(response => response.data)
}
