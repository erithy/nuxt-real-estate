import request from '@/api/request.js'

export default (keywords) => {
  const lang = (keywords.lang === 'en') ? 'ENGLISH' : (keywords.lang === 'kh') ? 'KHMER' : 'CHINESE'

  return request({
    data: {
      query: `
      {
        contact:KeyValues(where: {key: {EQ: "contact"} language: {EQ: ${lang}} }) {
          select {
            id
            key
            value
            jsonValue
            language
          }
        }
        logo:KeyValue(id:215) {
          id
          key
          value
          jsonValue
          language
        }
        header:KeyValues(where: {key: {EQ: "careerTitle"} language: {EQ: ${lang}} }) {
          select {
            id
            key
            value
            jsonValue
            language
          }
        }
        Careers(where: {language: {EQ: ${lang}}}){
          select{
            title
            id
            contact
            description
            salary
            profit
            requirement
            language
            brief
            requirePost
            info
          }
        }
      }
      `
    }
  }).then(response => response.data)
}
