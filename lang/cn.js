import vuetify from 'vuetify/es5/locale/en.js'

export default {
  $vuetify: vuetify,
  components: {
    home: {
      list: {
        showMore: '展开'
      },
      search: '搜索地产、新闻、活动',
      properties: '地产',
      newsAndEvents: '新闻及活动'
    },
    about: {
      teams: {
        title: '我们的团队'
      },
      certification: '我们的认证'
    },
    menu: {
      home: '首页',
      properties: '房地产',
      news: '新闻及活动',
      contact: '联系我们',
      career: '诚聘',
      about: '关于我们',
      services: '服务'
    },
    propertyDetails: {
      table: {
        price: '价格',
        type: '类型',
        parking: '停车场',
        area: '面积',
        bedroom: '卧室'
      }
    },
    news: {
      readMore: '阅读更多'
    },
    search: {
      commercial: '商务类',
      locations: '位置',
      propertyType: '房地产类型',
      price: '价格',
      search: '搜索',
      rent: '出租',
      sale: '出售'
    },
    footer: {
      followUs: '社交媒体',
      contactUs: '联系我们'
    }
  },
  views: {
    home: {
      recent: '最近',
      featured: '精选',
      latestNews: '近期新闻及活动'
    },
    news: {
      latestNews: '近期新闻',
      featured: '特色',
      noData: '找不到新闻与活动'
    },
    properties: {
      noData: '找不到数据'
    },
    propertyDetails: {
      location: '位置',
      propertyFeature: '房地产',
      bedRoom: '卧室',
      area: '面积',
      park: '停车场',
      storey: '楼层',
      bathRoom: '浴室',
      kitchen: '厨房',
      back: '回去'
    },
    career: {
      openPositions: '需求职位',
      jobTitle: '工作岗位',
      post: '数量',
      benefits: '待遇',
      responsibilities: '义务和责任',
      requirements: '技能及要求',
      noData: '没有诚聘信息'
    },
    contact: {
      getMoreInfo: '获得更多信息',
      name: '姓名',
      email: '邮箱 *',
      contactNum: '联系号码',
      subject: '主题',
      des: '内容',
      submit: '提交',
      required: '内容必填',
      validEmail: '邮箱格式不正确',
      sent: '消息不能发送',
      tryAgain: '请稍候再试',
      ourLocation: '我们的位置',
      address: '地址',
      contact: '联系方式'

    }
  }
}
