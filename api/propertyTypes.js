import request from '@/api/request.js'

export default (keywords) => {
  const lang = (keywords.lang === 'en') ? 'ENGLISH' : (keywords.lang === 'kh') ? 'KHMER' : 'CHINESE'

  return request({
    data: {
      query: `
      {
        contact:KeyValues(where: {key: {EQ: "contact"} language: {EQ: ${lang}} }) {
          select {
            id
            key
            value
            jsonValue
            language
          }
        }
        logo:KeyValue(id:215) {
          id
          key
          value
          jsonValue
          language
        }
        ProvinceDistricts(where: {language: {EQ: ${lang}} }) {
          select {
            id
            name
            description
            language
          }
        }
        PropertyTypes(where: {language: {EQ: ${lang}} }) {
          select {
            name
            id
            language
            image
            description
          }
        }
      }
      `
    }
  }).then(response => response.data)
}
