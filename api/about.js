import request from '@/api/request.js'

export default (keywords) => {
  const lang = (keywords.lang === 'en') ? 'ENGLISH' : (keywords.lang === 'kh') ? 'KHMER' : 'CHINESE'

  return request({
    data: {
      query: `
      {
        contact:KeyValues(where: {key: {EQ: "contact"} language: {EQ: ${lang}} }) {
          select {
            id
            key
            value
            jsonValue
            language
          }
        }
        logo:KeyValue(id:215) {
          id
          key
          value
          jsonValue
          language
        }
        header:KeyValues(where: {key: {EQ: "newsTitle"} language: {EQ: ${lang}} }) {
          select {
            id
            key
            value
            jsonValue
            language
          }
        }
        certification:KeyValues(where: {key: {EQ: "certification"} language: {EQ: ${lang}} }) {
          select {
            id
            key
            value
            jsonValue
            language
          }
        }
        mission:KeyValues(where: {key: {EQ: "mission"} language: {EQ: ${lang}} }) {
          select {
            id
            key
            value
            jsonValue
            language
          }
        }
        aboutVision:KeyValues(where: {key: {EQ: "aboutVision"} language: {EQ: ${lang}} }) {
          select {
            id
            key
            value
            jsonValue
            language
          }
        }
        TeamMembers(where: {language: {EQ: ${lang}}}) {
          select {
            tel
            name
            position
            email
            socialLink
            language
            image
          }
        }
        CompanyHistories(where: {language: {EQ: ${lang}}}) {
          select {
            description
            image
            language
            year
            id
          }
        }
      }
      `
    }
  }).then(response => response.data)
}
