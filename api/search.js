import request from '@/api/request.js'

export default (keyword) => {
  return request({
    method: 'get',
    baseURL: `${process.env.baseUrl}/search?keyword=${keyword}`,
    timeout: 4000
  })
}
