import vuetify from 'vuetify/es5/locale/en.js'

export default {
  $vuetify: vuetify,
  components: {
    home: {
      list: {
        showMore: 'បង្ហាញថែមទៀត'
      },
      search: 'ស្វែងរក ទីតាំង, ពត៌មាន, ព្រីត្តិការណ៍',
      properties: 'លំនៅដ្ឋាន',
      newsAndEvents: 'ពត៌មាន និង ព្រីត្តិការណ៍'
    },
    about: {
      teams: {
        title: 'ក្រុមការងារ'
      },
      certification: 'វិញ្ញាបនប័ត្រ'
    },
    menu: {
      home: 'ទំព័រដើម',
      properties: 'លំនៅដ្ឋាន',
      news: 'ព័ត៍មាន',
      contact: 'ទំនាក់ទំនង',
      career: 'ការងារ',
      about: 'អំពី',
      services: 'សេវាកម្ម'
    },
    propertyDetails: {
      table: {
        price: 'តម្លៃ',
        type: 'ប្រភេទ',
        parking: 'កន្លែងចត',
        area: 'ទំហំផ្ទៃ',
        bedroom: 'បន្ទប់គេង'
      }
    },
    news: {
      readMore: 'អានបន្ថែមទៀត'
    },
    search: {
      commercial: 'ប្រភេទពាណិជ្ជកម្ម',
      locations: 'ទីកន្លែង',
      propertyType: 'ប្រភេទលំនៅដ្ឋាន',
      price: 'តម្លៃ',
      search: 'ស្វែងរក',
      rent: 'ជួល',
      sale: 'លក់'
    },
    footer: {
      followUs: 'ប្រព័ន្ធផ្សព្វផ្សាយសង្គម',
      contactUs: 'ទំនាក់ទំនង'
    }
  },
  views: {
    home: {
      recent: 'ថ្មីៗ',
      featured: 'លក្ខណៈពិសេស',
      latestNews: 'ព័ត៍មានថ្មីៗ'
    },
    news: {
      latestNews: 'ព័ត៍មានថ្មីៗ',
      featured: 'លក្ខណៈពិសេស',
      noData: 'គ្មានទិន្នន័យ'
    },
    properties: {
      noData: 'គ្មានទិន្នន័យ'
    },
    propertyDetails: {
      location: 'ទីកន្លែង',
      propertyFeature: 'លក្ខណៈពិសេស',
      bedRoom: 'បន្ទប់គេង',
      area: 'ទំហំផ្ទៃ',
      park: 'កន្លែងចត',
      storey: 'ជាន់',
      bathRoom: 'បន្ទប់ទឹក',
      kitchen: 'ផ្ទះបាយ',
      back: 'ត្រលប់ក្រោយ'
    },
    career: {
      openPositions: 'មុខដំណែង',
      jobTitle: 'មុខងារ',
      post: 'ដំណែង',
      benefits: 'អត្ថប្រយោជន៍សម្រាប់បេក្ខជនដែលមានសមត្ថភាព',
      responsibilities: 'កាតព្វកិច្ចនិងទំនួលខុសត្រូវ',
      requirements: 'ជំនាញនិងតម្រូវការ',
      noData: 'មិនមានមុខដំណែង'
    },
    contact: {
      getMoreInfo: 'ទទួលបានព័ត៌មានបន្ថែម',
      name: 'ឈ្មោះ',
      email: 'អុីមែល *',
      contactNum: 'លេខទំនាក់ទំនង',
      subject: 'ប្រធានបទ',
      des: 'ការពិពណ៌នា',
      submit: 'ដាក់ស្នើ',
      required: 'តំរូវអោយបំពេញ',
      validEmail: 'អុីមែលមិនត្រឹមត្រូវ',
      sent: 'សារត្រូវបានបញ្ចូន',
      tryAgain: 'សូម​ព្យាយាម​ម្តង​ទៀត​នៅ​ពេល​ក្រោយ',
      ourLocation: 'ទីតាំង',
      address: 'អាសយដ្ឋាន',
      contact: 'ទំនាក់ទំនង'
    }
  }
}
