import request from '@/api/request.js'

export default (keywords) => {
  const lang = (keywords.lang === 'en') ? 'ENGLISH' : (keywords.lang === 'kh') ? 'KHMER' : 'CHINESE'

  return request({
    data: {
      query: `
      { contact:KeyValues(where: {key: {EQ: "contact"} language: {EQ: ${lang}} }) {
          select {
            id
            key
            value
            jsonValue
            language
          }
        }
        logo:KeyValue(id:215) {
          id
          key
          value
          jsonValue
          language
        }
        header:KeyValues(where: {key: {EQ: "servicesTitle"} language: {EQ: ${lang}} }) {
          select {
            id
            key
            value
            jsonValue
            language
          }
        }
          Services(where: {language: {EQ: ${lang}}}) {
            select{
              id
              info
              content
              language
              description
              bgImage
              title
              updatedAt
              createdAt
            }
          }
      }
      `
    }
  }).then(response => response.data)
}
