import request from '@/api/request.js'

export default (form) => {
  return request({
    method: 'post',
    baseURL: `${process.env.baseUrl}/feedback`,
    timeout: 4000,
    headers: {
      'Content-Type': 'application/json'
    },
    data: form
  }).then(response => response.data)
}
