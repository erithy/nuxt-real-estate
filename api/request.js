import axios from 'axios'

const instance = axios.create({
  method: 'post',
  baseURL: `${process.env.baseUrl}/graphql`,
  timeout: 4000
})

export default instance
