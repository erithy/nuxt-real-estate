import request from '@/api/request.js'

export default (keywords) => {
  const lang = (keywords.lang === 'en') ? 'ENGLISH' : (keywords.lang === 'kh') ? 'KHMER' : 'CHINESE'
  return request({
    data: {
      query: `
      {
        contact:KeyValues(where: {key: {EQ: "contact"} language: {EQ: ${lang}} }) {
          select {
            id
            key
            value
            jsonValue
            language
          }
        }
        logo:KeyValue(id:215) {
          id
          key
          value
          jsonValue
          language
        }
        Property(id: ${keywords.id}){
            id
            price
            info
            propertyFeature {
              bedRoom
              park
              storey
              bathRoom
              area
              kitchen
            }
            agent {
              id
              email
              image
              name
              tel
              position
            }
            location{
              longitude
              latitude
            }
            bgImage
            images
            description
            address
            name
            currency
            type{
              id
              name
            }
            commercialType
            createdAt
            priority
            brief
            language
          }
      }
      `
    }
  }).then(response => response.data)
}
