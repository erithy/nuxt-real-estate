import vuetify from 'vuetify/es5/locale/en.js'

export default {
  $vuetify: vuetify,
  components: {
    home: {
      list: {
        showMore: 'Show more'
      },
      search: 'Search for Address, News, Events',
      properties: 'Properties',
      newsAndEvents: 'News and Events'
    },
    about: {
      teams: {
        title: 'Our Teams'
      },
      certification: 'Our Company'
    },
    menu: {
      home: 'Home',
      properties: 'Properties',
      news: 'News',
      contact: 'Contact',
      career: 'Career',
      about: 'About',
      services: 'Services'
    },
    propertyDetails: {
      table: {
        price: 'Price',
        type: 'Type',
        parking: 'Parking space',
        area: 'Area size',
        bedroom: 'Bedrooms'
      }
    },
    news: {
      readMore: 'Read more'
    },
    search: {
      commercial: 'Commercial',
      locations: 'Locations',
      propertyType: 'Category',
      price: 'Price',
      search: 'Search',
      sale: 'Sale',
      rent: 'Rent'
    },
    footer: {
      followUs: 'Follow us',
      contactUs: 'Contact Us'
    }
  },
  views: {
    home: {
      recent: 'Recent',
      featured: 'Featured',
      latestNews: 'Latest News'
    },
    news: {
      latestNews: 'Latest News',
      featured: 'Featured',
      noData: 'No news or events found'
    },
    properties: {
      noData: 'No data found'
    },
    propertyDetails: {
      location: 'Location',
      propertyFeature: 'Property Features',
      bedRoom: 'Bedroom(s)',
      area: 'Areas',
      park: 'Car Parking',
      storey: 'Storey',
      bathRoom: 'Bathroom',
      kitchen: 'Kitchen',
      back: 'Back'
    },
    career: {
      openPositions: 'Open Positions',
      jobTitle: 'Job title',
      post: 'Post',
      benefits: 'Benefits for the qualified candidate',
      responsibilities: 'Duties and Responsibilities',
      requirements: 'Skills and Requirement',
      noData: 'No position available'
    },
    contact: {
      getMoreInfo: 'Get More Informations',
      name: 'Name',
      email: 'E-mail *',
      contactNum: 'Contact number',
      subject: 'Subject',
      des: 'Descrptions',
      submit: 'Submit',
      required: 'Field is required',
      validEmail: 'E-mail must be valid',
      sent: 'Message has been sent',
      tryAgain: 'Please try again later',
      ourLocation: 'Our Location',
      address: 'Address',
      contact: 'Contact'

    }
  }
}
